use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::subclass::prelude::*;
use gtk::NoSelection;
use gtk::{gio, glib};
use once_cell::sync::OnceCell;

use crate::application::ExampleApplication;
use crate::config::{APP_ID, PROFILE};

use crate::app::components::card::{CardModel, CardWidget};

mod imp {
    use super::*;

    use gtk::CompositeTemplate;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/space/tjo/dayzlauncher/ui/window.ui")]
    pub struct ExampleApplicationWindow {
        #[template_child]
        pub headerbar: TemplateChild<adw::HeaderBar>,
        #[template_child]
        pub favorites_list: TemplateChild<gtk::ListView>,
        pub model: OnceCell<gio::ListStore>,
        pub settings: gio::Settings,
    }

    impl Default for ExampleApplicationWindow {
        fn default() -> Self {
            Self {
                headerbar: TemplateChild::default(),
                favorites_list: TemplateChild::default(),
                model: OnceCell::default(),
                settings: gio::Settings::new(APP_ID),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ExampleApplicationWindow {
        const NAME: &'static str = "ExampleApplicationWindow";
        type Type = super::ExampleApplicationWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        // You must call `Widget`'s `init_template()` within `instance_init()`.
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ExampleApplicationWindow {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            // Devel Profile
            if PROFILE == "Devel" {
                obj.add_css_class("devel");
            }

            // Load latest window state
            obj.load_window_size();

            // Setup
            obj.setup_model();
            obj.setup_callbacks();
            obj.setup_factory();
        }
    }

    impl WidgetImpl for ExampleApplicationWindow {}
    impl WindowImpl for ExampleApplicationWindow {
        // Save window state on delete event
        fn close_request(&self, window: &Self::Type) -> gtk::Inhibit {
            if let Err(err) = window.save_window_size() {
                log::warn!("Failed to save window state, {}", &err);
            }

            // Pass close request on to the parent
            self.parent_close_request(window)
        }
    }

    impl AdwApplicationWindowImpl for ExampleApplicationWindow {}
    impl ApplicationWindowImpl for ExampleApplicationWindow {}
}

glib::wrapper! {
    pub struct ExampleApplicationWindow(ObjectSubclass<imp::ExampleApplicationWindow>)
        @extends gtk::Widget, gtk::Window, adw::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap, gtk::Accessible, gtk::Buildable,
                    gtk::ConstraintTarget, gtk::Native, gtk::Root, gtk::ShortcutManager;
}

impl ExampleApplicationWindow {
    pub fn new(app: &ExampleApplication) -> Self {
        glib::Object::new(&[("application", app)])
            .expect("Failed to create ExampleApplicationWindow")
    }

    fn model(&self) -> &gio::ListStore {
        // Get state
        let imp = imp::ExampleApplicationWindow::from_instance(self);
        imp.model.get().expect("Could not get model")
    }

    fn setup_model(&self) {
        // Create new model
        let model = gio::ListStore::new(CardModel::static_type());

        // Get state and set model
        let imp = imp::ExampleApplicationWindow::from_instance(self);
        imp.model.set(model).expect("Could not set model");

        // Wrap model with selection and pass it to the list view
        let selection_model = NoSelection::new(Some(self.model()));
        imp.favorites_list.set_model(Some(&selection_model));
    }

    fn setup_callbacks(&self) {
        // Get state
        let model = self.model();

        model.append(&CardModel::new(
            "dayz #1",
            "/space/tjo/dayzlauncher/headers/dayz.jpg",
        ));
        model.append(&CardModel::new(
            "livonia #1",
            "/space/tjo/dayzlauncher/headers/livonia.jpg",
        ));
        model.append(&CardModel::new(
            "livonia #2",
            "/space/tjo/dayzlauncher/headers/livonia.jpg",
        ));
        model.append(&CardModel::new(
            "dayz #2",
            "/space/tjo/dayzlauncher/headers/dayz.jpg",
        ));
    }

    fn setup_factory(&self) {
        // Create a new factory
        let factory = gtk::SignalListItemFactory::new();

        // Create an empty `TodoRow` during setup
        factory.connect_setup(move |_, list_item| {
            // Create `TodoRow`
            let card_widget = CardWidget::new();
            list_item.set_child(Some(&card_widget));
        });

        // Tell factory how to bind `CardWidget` to a `CardModel`
        factory.connect_bind(move |_, list_item| {
            // Get `CardModel` from `ListItem`
            let card_model = list_item
                .item()
                .expect("The item has to exist.")
                .downcast::<CardModel>()
                .expect("The item has to be an `CardModel`.");

            // Get `CardWidget` from `ListItem`
            let card_widget = list_item
                .child()
                .expect("The child has to exist.")
                .downcast::<CardWidget>()
                .expect("The child has to be a `CardWidget`.");

            card_widget.bind(&card_model);
        });

        // Tell factory how to unbind `TodoRow` from `TodoObject`
        factory.connect_unbind(move |_, list_item| {
            // Get `TodoRow` from `ListItem`
            let card_widget = list_item
                .child()
                .expect("The child has to exist.")
                .downcast::<CardWidget>()
                .expect("The child has to be a `CardWidget`.");

            card_widget.unbind();
        });

        // Set the factory of the list view
        let imp = imp::ExampleApplicationWindow::from_instance(self);
        imp.favorites_list.set_factory(Some(&factory));
    }

    fn save_window_size(&self) -> Result<(), glib::BoolError> {
        let self_ = imp::ExampleApplicationWindow::from_instance(self);

        let (width, height) = self.default_size();

        self_.settings.set_int("window-width", width)?;
        self_.settings.set_int("window-height", height)?;

        self_
            .settings
            .set_boolean("is-maximized", self.is_maximized())?;

        Ok(())
    }

    fn load_window_size(&self) {
        let self_ = imp::ExampleApplicationWindow::from_instance(self);

        let width = self_.settings.int("window-width");
        let height = self_.settings.int("window-height");
        let is_maximized = self_.settings.boolean("is-maximized");

        self.set_default_size(width, height);

        if is_maximized {
            self.maximize();
        }
    }
}
