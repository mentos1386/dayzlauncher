#![allow(clippy::all)]

use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use std::cell::RefCell;
use std::rc::Rc;

glib::wrapper! {
    pub struct CardModel(ObjectSubclass<imp::CardModel>);
}

// Constructor for new instances. This simply calls glib::Object::new() with
// initial values for our two properties and then returns the new instance
impl CardModel {
    pub fn new(title: &str, cover: &str) -> Self {
        glib::Object::new(&[("title", &title), ("cover", &cover)]).expect("Failed to create")
    }

    pub fn cover(&self) -> Option<String> {
        self.property("cover")
            .unwrap()
            .get::<&str>()
            .ok()
            .map(|s| s.to_string())
    }
}

#[derive(Default)]
pub struct CardData {
    pub title: String,
    pub cover: String,
}

mod imp {

    use super::*;

    // Static array for defining the properties of the new type.
    lazy_static! {
        static ref PROPERTIES: [glib::ParamSpec; 2] = [
            glib::ParamSpec::new_string("title", "Title", "", None, glib::ParamFlags::READWRITE),
            glib::ParamSpec::new_string("cover", "Cover", "", None, glib::ParamFlags::READWRITE),
        ];
    }

    // This is the struct containing all state carried with
    // the new type. Generally this has to make use of
    // interior mutability.
    #[derive(Default)]
    pub struct CardModel {
        pub data: Rc<RefCell<CardData>>,
    }

    // ObjectSubclass is the trait that defines the new type and
    // contains all information needed by the GObject type system,
    // including the new type's name, parent type, etc.
    #[glib::object_subclass]
    impl ObjectSubclass for CardModel {
        // This type name must be unique per process.
        const NAME: &'static str = "CardModel";

        type Type = super::CardModel;

        // The parent type this one is inheriting from.
        type ParentType = glib::Object;
    }

    // Trait that is used to override virtual methods of glib::Object.
    impl ObjectImpl for CardModel {
        fn properties() -> &'static [glib::ParamSpec] {
            &*PROPERTIES
        }

        // Called whenever a property is set on this instance. The id
        // is the same as the index of the property in the PROPERTIES array.
        fn set_property(
            &self,
            _obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "title" => {
                    let title = value
                        .get()
                        .expect("type conformity checked by `Object::set_property`");
                    self.data.borrow_mut().title = title;
                }
                "cover" => {
                    let cover = value
                        .get()
                        .expect("type conformity checked by `Object::set_property`");
                    self.data.borrow_mut().cover = cover;
                }
                _ => unimplemented!(),
            }
        }

        // Called whenever a property is retrieved from this instance. The id
        // is the same as the index of the property in the PROPERTIES array.
        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "title" => self.data.borrow().title.to_value(),
                "cover" => self.data.borrow().cover.to_value(),
                _ => unimplemented!(),
            }
        }
    }
}
