use adw::subclass::prelude::BinImpl;
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::CompositeTemplate;

use super::CardModel;

use crate::app::components::display_add_css_provider;

mod imp {

    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/space/tjo/dayzlauncher/components/card.ui")]
    pub struct CardWidget {
        #[template_child]
        pub title_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub cover_image: TemplateChild<gtk::Image>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for CardWidget {
        const NAME: &'static str = "CardWidget";
        type Type = super::CardWidget;
        type ParentType = adw::Bin;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for CardWidget {}
    impl WidgetImpl for CardWidget {}
    impl BinImpl for CardWidget {}
}

glib::wrapper! {
    pub struct CardWidget(ObjectSubclass<imp::CardWidget>) @extends gtk::Widget, adw::Bin;
}

impl CardWidget {
    pub fn new() -> Self {
        display_add_css_provider("space/tjo/dayzlauncher/components/card.css");
        glib::Object::new(&[]).expect("Failed to create an instance of CardWidget")
    }

    pub fn bind(&self, card_model: &CardModel) {
        let widget = imp::CardWidget::from_instance(self);

        card_model
            .bind_property("title", &*widget.title_label, "label")
            .flags(glib::BindingFlags::DEFAULT | glib::BindingFlags::SYNC_CREATE)
            .build();

        card_model.cover().map(|cover| {
            widget.cover_image.set_from_resource(Some(cover.as_str()));
        });
    }

    pub fn unbind(&self) {}
}
