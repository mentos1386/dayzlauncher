# DayZ Launcher

Launcher for DayZ game with server finder and mod support.

<div align="center">
![Main window](data/resources/screenshots/screenshot1.png "Main window")
</div>



## Building the project

```
flatpak install org.gnome.Sdk//41 org.freedesktop.Sdk.Extension.rust-stable//21.08 org.gnome.Platform//41
flatpak-builder --user flatpak_app build-aux/space.tjo.dayzlauncher.Devel.json
```

## Running the project

```
flatpak-builder --run flatpak_app build-aux/space.tjo.dayzlauncher.Devel.json DayzLauncher
```

